


function sendContactForm() {

    $.ajax({
    	type: 'post',
    	url: '/scripts/sendcontact.php',
    	data: $("#contato").serialize(),
    	success: function (result) {
			$(".spinner").fadeOut('fast');
			$(".response-form").html('Sua mensagem foi enviada com sucesso.<br/><br/>Por gentileza aguarde o retorno de um de nossos representantes.');
			$(".overflow").fadeIn('fast');
    	},
		error: function (err) {
			$(".spinner").fadeOut('fast');
			$(".response-form").html('Infelizmente tivemos um erro com o envio de sua mensagem.<br/><br/>Tente novamente mais tarde ou envie um email para: contato@bracafe.com.br');
			$(".overflow").fadeIn('fast');
		},
	});
}

$( document ).ready(function(){



    $(".overflow-close").on('click', function(event){
        $(".overflow").fadeOut('fast');
        event.preventDefault();
    });

    $(".overflow").on('click', function(event){
        $(".overflow").fadeOut('fast');
        event.preventDefault();
    });

    $("#enviar").on('click', function(){
        $(".overflow").fadeIn('fast');
    	sendContactForm();
    });

	  $(".menu-btn").click(function(){
    var divId = ($(this).attr('href'));
      $('html,body').animate({
          scrollTop: $(divId).offset().top
      },1500);
  });

	  $(".navbar-toggle").click(function(){
	  	$(".menu-mobile").fadeIn("fast");
	  });

	  $(".menu-mobile").click(function(){
	  	$(".menu-mobile").fadeOut("fast");
	  });
	  
	  $(".fa-times").click(function(){
	  	$(".menu-mobile").fadeOut("fast");
	  });


	$('.galeria').slick({
		dots: false,
		infinite: false,
		slidesToShow: 3,
		responsive: [
			{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
				dots: true
				}
			},
			{
			breakpoint: 600,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
				}
			},
			{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
				}
			}
		]
	});

});


$(window).scroll(function() {
	if ($(window).scrollTop() > ($('.sobre').offset().top)) {
		console.log( $('.sobre').offset().top + 100);
		console.log( $(window).scrollTop() );
		$('.navbar').animate().addClass('fixed');

	}else{
		$('.navbar').removeClass('fixed');
	}
});

