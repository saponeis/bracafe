<?php
require_once '../vendor/swiftmailer/lib/swift_required.php';

$return["json"] = json_encode( [
  'codigo' => 2,
  'successs'=> false,
  'error' => true
] );

if (!isset($_POST)) {
   echo json_encode($return);
   die();
}

if (!isset($_POST['email'])) {
   echo json_encode($return);
   die();
}

  $admin_email = "saponeis@gmail.com";
  $smtp = 'smtp.gmail.com';
  $pwd = 'Sh!n0S4n';
  $port = '465';


  $email = $_POST['email'];
  $nome = $_POST['nome'];
  $comentario = $_POST['mensagem'];
  
  $text = "Envio de contato - Site Simples" . PHP_EOL . PHP_EOL;
  $text.= "Nome: " . $nome . PHP_EOL;
  $text.= "E-Mail: " . $email . PHP_EOL;
  $text.= "Mensagem : " . $comentario . PHP_EOL;



// Create the Transport
$transport = Swift_SmtpTransport::newInstance($smtp, $port, 'ssl')
  ->setUsername($admin_email)
  ->setPassword($pwd);



$mailer = Swift_Mailer::newInstance($transport);

$message = Swift_Message::newInstance()

  // Give the message a subject
  ->setSubject( '[Simples] Formulário de Contato Completo' )

  // Set the From address with an associative array
  ->setFrom( ['noreply@simples.com.br' => 'Noreply Simples'] )

  // Set the To addresses with an associative array
  ->setTo( ['gusmao@simplesag.com.br','saponeis@gmail.com'] )

  // Give it a body
  ->setBody( $text );


$result = $mailer->send($message);

$return = [];

$return["json"] = json_encode( [
	'codigo' => 1,
	'successs'=> true,
	'error' => false
] );

echo json_encode($return);

?>